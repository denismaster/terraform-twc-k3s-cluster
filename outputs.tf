output "cluster_ip" {
  value = module.master.server_public_ip
}