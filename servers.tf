module "master" {
  source  = "deff-dev/cloud-server/timeweb"
  version = ">= v2.1.0"

  name          = "${var.name}-k3s-master"
  project_name  = var.master.project_name
  os            = var.master.os
  location      = var.master.location
  cpu_frequency = var.master.cpu_frequency
  disk_type     = var.master.disk_type

  preset         = var.master.preset
  configurator   = var.master.configurator
  ssh_keys_paths = var.master.ssh_keys_paths
  ssh_keys       = var.master.ssh_keys
}

module "workers" {
  source   = "deff-dev/cloud-server/timeweb"
  version  = ">= v2.1.0"
  for_each = var.workers

  name          = "${var.name}-k3s-worker-${index(keys(var.workers), each.key)}"
  project_name  = each.value.project_name
  os            = each.value.os
  location      = each.value.location
  cpu_frequency = each.value.cpu_frequency
  disk_type     = each.value.disk_type

  preset         = each.value.preset
  configurator   = each.value.configurator
  ssh_keys_paths = each.value.ssh_keys_paths
  ssh_keys       = each.value.ssh_keys
}