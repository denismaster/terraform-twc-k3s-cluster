# terraform-twc-k3s-cluster

## 🧐 О модуле
Модуль для Terraform, который позволяет развернуть кластер k3s в TimeWeb Cloud.

**Важно** - На данный момент поддерживается только кластер, состоящий из 1 ноды. Дополнительные агенты и серверы будут реализованы позже.

**Важно** - В отличие от ванильного k3s тут НЕ УСТАНАВЛИВАЮТСЯ traefik-ingress и servicelb.

## 📋 Требования <a name = "requirements"></a>

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](https://www.terraform.io/) | >= 1.3 |
| <a name="requirement_timeweb-cloud"></a> [timeweb-cloud](https://registry.terraform.io/providers/timeweb-cloud/timeweb-cloud/latest/docs/) | >= 1.2.1 |
| <a name="requirement_timeweb-cloud-server"></a> [timeweb-cloud-server](https://github.com/deff-dev/terraform-timeweb-cloud-server/tree/main) | >= 2.1.0 |
| <a name="requirement_k3sup"></a> [k3sup](https://github.com/alexellis/k3sup) | >= 0.13.5 |


## ⚙️ Установка NGINX Ingress в созданный кластер

```bash
helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace --set controller.hostNetwork=true,controller.service.type="",controller.kind=DaemonSet
```

## ✍️ Авторы <a name = "authors"></a>

Модуль поддерживается [denismaster](https://gitlab.com/denismaster). <br>
Спасибо [Deff](https://github.com/deff-dev) за тесты и моральную поддержку.