module "k3s_cluster" {
  source = "../"

  name            = var.name
  ssh_private_key = var.ssh_private_key
  kubeconfig_path = var.kubeconfig_path

  master = var.master
  workers = var.workers
}