terraform {
  required_providers {
    twc = {
      source  = "timeweb-cloud/timeweb-cloud"
      version = ">= 1.2.1"
    }
  }
}

provider "twc" {
  token = var.timeweb_token
  alias = "default"
}